// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ReCaptcha",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "ReCaptcha",
            targets: ["ReCaptcha"]),
    ],
    dependencies: [
        .package(url: "https://github.com/ReactiveX/RxSwift.git", exact: "5.1.2"),
        .package(url: "https://github.com/RxSwiftCommunity/RxDataSources.git", exact: "4.0.0")
        
    ],
    targets: [
        .target(
            name: "ReCaptcha",
            dependencies: [
                "RxSwift",
                "RxDataSources"
            ],
            resources: [
                .process("recaptcha.html")
            ]
        ),
    ]
)
