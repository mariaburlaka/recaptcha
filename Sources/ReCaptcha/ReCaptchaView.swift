//
//  ReCaptchaView.swift
//  licard-b2c
//
//  Created by Maria Selemeneva on 17/04/2023.
//  Copyright © 2022 LICARD. All rights reserved.
//

import UIKit
import WebKit
import RxCocoa
import RxSwift

public struct ReCaptchaModel {
    /// ReCaptcha api key
    var reCaptchaApiKey: String
    /// Site url with format: "https://example.com"
    var reCaptchaDomain: String
    /// Completion with receivedToken value after get it from webView
    let completion: ((String) -> Void)?

    public init(reCaptchaApiKey: String,
                reCaptchaDomain: String,
                completion: ((String) -> Void)? = nil) {
        self.reCaptchaApiKey = reCaptchaApiKey
        self.reCaptchaDomain = reCaptchaDomain
        self.completion = completion
    }
}

final public class ReCaptchaView: UIView {
    // MARK: - PrivateProperties
    private let bag = DisposeBag()
    private let receivedToken = BehaviorRelay<String>(value: "")
    private var siteKey: String = ""
    private var url: URL?
    private var webView = WKWebView()
    
    private var html: String {
        guard let filePath = Bundle.module.url(forResource: "recaptcha", withExtension: "html")?.path else {
            assertionFailure("Error: Unable to find the file recaptcha.html")
            return ""
        }

        guard let contents = try? String(contentsOfFile: filePath, encoding: .utf8) else {
            fatalError()
        }

        return parse(contents, with: ["siteKey": siteKey])
    }
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func configure(model: ReCaptchaModel) {
        siteKey = model.reCaptchaApiKey
        url = URL(string: model.reCaptchaDomain)
    
        if let url = url {
            webView.loadHTMLString(html, baseURL: url)
        }
        
        receivedToken.subscribe(onNext: { [weak self] token in
            guard !token.isEmpty else { return }
            
            /// Нужна задержка, чтобы экран закрылся после успешной анимации капчи
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                model.completion?(self?.receivedToken.value ?? "")
            }
        }).disposed(by: bag)
    }
}

// MARK: - View Setup
extension ReCaptchaView {
    private func setupView() {
        webView = setupWebView()
        addSubview(webView)
        
        setupConstraints()
    }

    private func setupConstraints() {
        webView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate(
            [
                webView.leadingAnchor.constraint(equalTo: leadingAnchor),
                webView.trailingAnchor.constraint(equalTo: trailingAnchor),
                webView.topAnchor.constraint(equalTo: topAnchor),
                webView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ]
        )
        
    }
    
    private func setupWebView() -> WKWebView {
        let webConfiguration = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        
        contentController.add(self, name: "recaptcha")
        webConfiguration.userContentController = contentController
        
        return WKWebView(frame: .zero, configuration: webConfiguration)
    }
    
    private func parse(_ string: String, with valueMap: [String: String]) -> String {
        var parsedString = string

        valueMap.forEach { key, value in
            parsedString = parsedString.replacingOccurrences(
                of: "${\(key)}", with: value
            )
        }

        return parsedString
    }
}

// MARK: - WKScriptMessageHandler
extension ReCaptchaView: WKScriptMessageHandler {
    public func userContentController(_ userContentController: WKUserContentController,
                                      didReceive message: WKScriptMessage) {
        guard let message = message.body as? String else {
            assertionFailure("Expected a string message")
            return
        }

        receivedToken.accept(message)
    }
}
